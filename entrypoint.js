const { strict: assert } = require('assert')
const { existsSync } = require('fs')
const path = require('path')

const { ISSUER: issuer, VARIANT, MODULE_NAME: moduleName } = process.env

assert(issuer, 'process.env.ISSUER environment variable missing')
assert(VARIANT, 'process.env.VARIANT environment variable missing')
assert(moduleName, 'process.env.MODULE_NAME environment variable missing')

let variant
try {
  variant = JSON.parse(VARIANT)
} catch (err) {}

assert(variant, 'failed to JSON.parse process.env.VARIANT')

console.log('ISSUER:', issuer)
console.log('MODULE_NAME:', moduleName)
console.log('VARIANT:', variant)

const moduleFile = path.join(__dirname, 'modules', `${moduleName}.js`)
if (!existsSync(moduleFile)) {
  console.log(`missing test file: ${moduleFile}`)
  process.exitCode = 0
} else {
  const moduleDefinition = require(moduleFile)

  ;(async () => {
    await moduleDefinition({ issuer, variant })
    console.log('COMPLETED')
  })().catch((err) => {
    console.log('ERROR')
    console.log(err)
    process.exitCode = 1
  })
}
