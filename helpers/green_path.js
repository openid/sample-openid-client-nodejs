/* eslint-disable camelcase */
const fs = require('fs');
const querystring = require('querystring')
const { parse: parseUrl } = require('url') // eslint-disable-line node/no-deprecated-api

const jose = require('jose')
const { Issuer, generators, custom } = require('openid-client')
const got = require('got')

custom.setHttpOptionsDefaults({ timeout: 15000, retry: 2 })

const hooks = require('./hooks')
const needsJWKS = require('./needs_jwks')
const requestUri = require('./request_uri')
const testFunctions = require('./test_functions')

const envDefaults = (env) => {
  try {
    return JSON.parse(env)
  } catch (err) {}
}

async function greenPath ({ issuer: identifier, variant, params, metadata, userinfoArgs = [], forceNonce = false, skipUserinfo = false }) {
  let issuerInfo = await testFunctions.discover(identifier);

  let client = await testFunctions.registerClient({issuer:issuerInfo, variant:variant, metadata:metadata });
  
  let tokens = await testFunctions.authorize({variant, client, params, forceNonce});

  const {userinfo, claims} = await testFunctions.userinfo({client, tokens, skipUserinfo, userinfoArgs});

  return { tokens, userinfo, claims, client }

}

module.exports = greenPath
