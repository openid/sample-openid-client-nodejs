const { custom } = require('openid-client')

let hooks

if (process.env.DEBUG_OPENID_CLIENT) {
  hooks = {
    beforeRequest: [
      (options) => {
        console.log('--> %s %s', options.method.toUpperCase(), options.href)
        console.log('--> HEADERS %o', options.headers)
        if (options.body) {
          console.log('--> BODY %s', options.body)
        }
      }
    ],
    afterResponse: [
      (response) => {
        console.log('<-- %i FROM %s %s', response.statusCode, response.request.gotOptions.method.toUpperCase(), response.request.gotOptions.href)
        console.log('<-- HEADERS %o', response.headers)
        if (response.body) {
          console.log('<-- BODY %s', response.body)
        }
        return response
      }
    ]
  }
  custom.setHttpOptionsDefaults({ hooks })
}

module.exports = hooks
