const greenPath = require('./green_path')
const refreshToken = require('./refresh_token')
const {  discover, registerClient, authorize, userinfo } = require('./test_functions')
module.exports = {
  greenPath,
  refreshToken,
  discover,
  registerClient,
  authorize,
  userinfo
}
