async function refreshToken ({ client, tokens, userinfoArgs = [] }) {
  tokens = await client.refresh(tokens)

  let userinfo
  if (tokens.access_token) {
    userinfo = await client.userinfo(tokens, ...userinfoArgs)
    await client.unpackAggregatedClaims(userinfo)
    await client.fetchDistributedClaims(userinfo)
  }
  const claims = tokens.claims()
  await client.unpackAggregatedClaims(claims)
  await client.fetchDistributedClaims(claims)

  return { tokens, userinfo, claims, client }
}

module.exports = refreshToken
