/* eslint-disable camelcase */
const fs = require('fs');
const querystring = require('querystring')
const { parse: parseUrl } = require('url') // eslint-disable-line node/no-deprecated-api

const jose = require('jose')
const { Issuer, generators, custom } = require('openid-client')
const got = require('got')

custom.setHttpOptionsDefaults({ timeout: 15000, retry: 2 })

const hooks = require('./hooks')
const needsJWKS = require('./needs_jwks')
const requestUri = require('./request_uri')

const envDefaults = (env) => {
  try {
    return JSON.parse(env)
  } catch (err) {}
}

async function discover (identifier) {
    const discovered = await Issuer.discover(identifier)
    return discovered;
}

async function registerClient ({ issuer, variant, metadata}) {
  const {
    client_auth_type: token_endpoint_auth_method,
    request_type,
    response_type
  } = variant

  let {
    response_mode
  } = variant

  if (response_mode === 'default') {
    response_mode = undefined
  }

  const {
    CLIENT_METADATA_DEFAULTS,
    POST
  } = process.env

  const clientMetadata = {
    request_object_signing_alg: request_type !== 'plain_http_request' ? 'RS256' : undefined,
    ...envDefaults(CLIENT_METADATA_DEFAULTS),
    token_endpoint_auth_method,
    response_types: [response_type],
    grant_types: [...response_type.split(' ').reduce((acc, type) => {
      switch (type) {
        case 'code':
          acc.add('authorization_code')
          break
        case 'id_token':
        case 'token':
          acc.add('implicit')
          break
        default:
      }
      return acc
    }, new Set())],
    redirect_uris: ['https://openid-client.local/cb'],
    ...metadata
  }

  let jwks
  if (needsJWKS(clientMetadata)) {
    const keystore = new jose.JWKS.KeyStore()
    await Promise.all([
      keystore.generate('EC', 'P-256', { use: 'sig' }),
      keystore.generate('EC', 'P-256', { use: 'enc' }),
      keystore.generate('RSA', 2048, { use: 'sig' }),
      keystore.generate('RSA', 2048, { use: 'enc' })
    ])
    //Add the configured key to jwks
    if(variant.client_auth_type=='self_signed_tls_client_auth'){
      const clientCertPEM = fs.readFileSync(process.env.CLIENT_CERT,'utf8');
      const clientCertKeyPEM = fs.readFileSync(process.env.CLIENT_CERT_KEY,'utf8');
      const jwkForClientCertKey = jose.JWK.asKey(clientCertKeyPEM);
      const jwkForClientCert = jose.JWK.asKey(clientCertPEM);
      const jwkPrivateKey = jwkForClientCertKey.toJWK(true);
      const jwkCert = jwkForClientCert.toJWK();
      jwkPrivateKey.x5c = jwkCert.x5c;
      jwkPrivateKey.x5t = jwkCert.x5t;
      jwkPrivateKey["x5t#S256"] = jwkCert["x5t#S256"];
      keystore.add(jose.JWK.asKey(jwkPrivateKey));
    }
    jwks = keystore.toJWKS(true)
  }


  const client = await issuer.Client.register(clientMetadata, { jwks })
  client[custom.clock_tolerance] = 5

  if(variant.client_auth_type=='tls_client_auth' || variant.client_auth_type=='self_signed_tls_client_auth'){
    client[custom.http_options] = function (options) {
      const clientCert = fs.readFileSync(process.env.CLIENT_CERT,'utf8');
      const clientCertKey = fs.readFileSync(process.env.CLIENT_CERT_KEY,'utf8');
      options.cert = clientCert;
      options.key = clientCertKey;
      return options;
    };
  }

  return client
}

async function authorize ({ variant, client, params, forceNonce = false, timekeeper=null }) {
  const {
    client_auth_type: token_endpoint_auth_method,
    request_type,
    response_type
  } = variant

  let {
    response_mode
  } = variant

  if (response_mode === 'default') {
    response_mode = undefined
  }
  const {
    CLIENT_METADATA_DEFAULTS,
    POST
  } = process.env

  let authorizationParams = {
    redirect_uri: client.redirect_uris[0],
    scope: 'openid',
    response_type,
    client_id: client.client_id,
    state: generators.state(),
    nonce: (response_type.includes('id_token') || forceNonce === true) ? generators.nonce() : undefined,
    response_mode,
    ...params
  }

  Object.entries(authorizationParams).forEach((key, value) => {
    if (value === null || value === undefined || value === '') {
      delete authorizationParams[key]
    }
  })

  const { state, nonce, redirect_uri } = authorizationParams

  switch (request_type) {
    case 'plain_http_request':
      break
    case 'request_object': {
      const requestObject = await client.requestObject(authorizationParams)
      authorizationParams = {
        scope: 'openid',
        response_type: authorizationParams.response_type,
        redirect_uri: undefined,
        request: requestObject
      }
      break
    }
    case 'request_uri': {
      const requestObject = await client.requestObject(authorizationParams)
      const request_uri = await requestUri(requestObject)
      authorizationParams = {
        scope: 'openid',
        response_type: authorizationParams.response_type,
        redirect_uri: undefined,
        request_uri
      }
      break
    }
    default:
      throw new TypeError(`unknown request_type ${request_type}`)
  }

  let authorization
  if (['1', 1, 'true', true].includes(POST)) {
    const url = client.issuer.authorization_endpoint
    const { query } = parseUrl(client.authorizationUrl(authorizationParams), true)
    authorization = await got.post(url, { followRedirect: false, hooks, form: true, body: query })
  } else {
    const url = client.authorizationUrl(authorizationParams)
    authorization = await got.get(url, { followRedirect: false, hooks })
  }

  let callbackParams
  switch (response_mode) {
    case 'query.jwt':
    case 'fragment.jwt':
    case 'form_post.jwt':
    case 'jwt':
    case 'web_message':
    case 'web_message.jwt':
      throw new Error('TODO')
    case 'form_post':
      authorization.method = 'POST'
      authorization.body = querystring.stringify(
        authorization.body.match(/<input type="hidden" name="\w+" value=".+"\/?>/g).reduce((acc, match) => {
          const [, key, value] = match.match(/name="(\w+)" value="(.+)"/)
          acc[key] = value
          return acc
        }, {})
      )
      callbackParams = client.callbackParams(authorization)
      break
    case 'query':
    case 'fragment':
    case undefined: {
      const { headers: { location } } = authorization;
      ({ query: callbackParams } = parseUrl(location.replace('#', '?'), true))
      break
    }
    default:
      throw new TypeError(`unknown response_type ${response_type}`)
  }
  if(timekeeper){
    console.log("Before timekeeper.travel: " + Date.now());
    await timekeeper.travel(Date.now() + (61 * 1000)); // travel one minute from now, making the cached keystore stale
    console.log("After timekeeper.travel:" + Date.now());
  }

  const tokens = await client.callback(redirect_uri, callbackParams, { response_type, nonce, state })
  return tokens;
}

async function userinfo ({ client, tokens, skipUserinfo, userinfoArgs = [] }) {
  let userinfo
  if (tokens.access_token && !skipUserinfo) {
    userinfo = await client.userinfo(tokens, ...userinfoArgs)
    await client.unpackAggregatedClaims(userinfo)
    await client.fetchDistributedClaims(userinfo)
  }
  const claims = tokens.claims()
  await client.unpackAggregatedClaims(claims)
  await client.fetchDistributedClaims(claims)

  return {userinfo, claims};
}

module.exports = {
    discover,
    registerClient,
    authorize,
    userinfo
}
