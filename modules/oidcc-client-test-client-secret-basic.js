const { strict: assert } = require('assert')

const helpers = require('../helpers')

module.exports = async ({ variant, issuer }) => {
  return assert.doesNotReject(
    helpers.greenPath({
      issuer,
      variant,
      metadata: {
        token_endpoint_auth_method: 'client_secret_basic'
      }
    })
  )
}
