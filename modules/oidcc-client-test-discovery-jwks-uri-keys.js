const { strict: assert } = require('assert')

const helpers = require('../helpers')

module.exports = async ({ variant, issuer }) => {
    let theflow = async function(){
        let issuerInfo = await helpers.discover(issuer);
        return issuerInfo.keystore();
    }
  
  return assert.doesNotReject(
    theflow()
  );
}


