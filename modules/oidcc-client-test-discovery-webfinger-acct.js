const { strict: assert } = require('assert')

const helpers = require('../helpers')
const { Issuer } = require('openid-client')
const url = require('url');

module.exports = async ({ variant, issuer }) => {
    let theflow = async function(){        
        let parsedUrl = url.parse(issuer);
        let alias = parsedUrl.path.replace('/test/a/','').replace('/','');
        let resource = 'acct:' + alias + '.oidcc-client-test-discovery-webfinger-acct@' + parsedUrl.host;
        console.log("resource for webfinger query=" + resource);
        await Issuer.webfinger(resource);
    }
  
  return assert.doesNotReject(
    theflow()
  );
}


