const { strict: assert } = require('assert')

const helpers = require('../helpers')
const { Issuer } = require('openid-client')

module.exports = async ({ variant, issuer }) => {
    let theflow = async function(){
        let resource = issuer + 'oidcc-client-test-discovery-webfinger-url';
        await Issuer.webfinger(resource);
    }
  
  return assert.doesNotReject(
    theflow()
  );
}


