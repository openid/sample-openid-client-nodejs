const { strict: assert } = require('assert')

const helpers = require('../helpers')

module.exports = async ({ variant, issuer }) => {
  const { tokens, claims, userinfo } = await helpers.greenPath({
    issuer,
    variant,
    params: {
      scope: 'openid email'
    }
  })

  //this test is not enabled for id_token response_type
  assert(userinfo.credit_score);
}
