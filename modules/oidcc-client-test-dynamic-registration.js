const { strict: assert } = require('assert')

const helpers = require('../helpers')

module.exports = async ({ variant, issuer }) => {
    let theflow = async function(){
        let issuerInfo = await helpers.discover(issuer);
        return helpers.registerClient({issuer:issuerInfo, variant:variant });        
    }
  
  return assert.doesNotReject(
    theflow()
  );
}


