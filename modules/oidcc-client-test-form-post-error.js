const { strict: assert } = require('assert')

const helpers = require('../helpers')

module.exports = async ({ variant, issuer }) => {
  return assert.rejects(
    helpers.greenPath({
      issuer,
      variant,
      params: {
        max_age: 0,
        prompt: 'none'
      }
    }),
    {
      name: 'OPError',
      error: 'login_required',
      error_description: 'This is a login_required error response'
    }
  )
}
