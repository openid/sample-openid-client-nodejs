const { strict: assert } = require('assert')

const helpers = require('../helpers')

module.exports = async ({ variant, issuer }) => {
  return assert.rejects(
    helpers.greenPath({
      issuer,
      variant
    }),
    {
      name: 'RPError',
      message: /^at_hash mismatch, expected (?<atHash>.+), got: \k<atHash>1/
    }
  )
}
