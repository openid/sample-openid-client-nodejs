const { strict: assert } = require('assert')

const helpers = require('../helpers')

module.exports = async ({ variant, issuer }) => {
  return assert.rejects(
    helpers.greenPath({
      issuer,
      variant,
      metadata: {
        id_token_signed_response_alg: 'RS256'
      }
    }),
    {
      name: 'RPError',
      message: 'failed to validate JWT signature'
    }
  )
}
