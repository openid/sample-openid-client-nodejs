const { strict: assert } = require('assert')

const helpers = require('../helpers')

module.exports = async ({ variant, issuer }) => {
  return assert.rejects(
    helpers.greenPath({
      issuer,
      variant
    }),
    {
      name: 'RPError',
      message: 'multiple matching keys found in issuer\'s jwks_uri for key parameters {"alg":"RS256"}, kid must be provided in this case'
    }
  )
}
