const { strict: assert } = require('assert')

const helpers = require('../helpers')

module.exports = async ({ variant, issuer }) => {
  return assert.rejects(
    helpers.greenPath({
      issuer,
      variant,
      forceNonce: true
    }),
    {
      name: 'RPError',
      message: /^nonce mismatch, expected (?<nonce>.+), got: \k<nonce>1$/
    }
  )
}
