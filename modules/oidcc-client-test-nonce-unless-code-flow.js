const { strict: assert } = require('assert')

const helpers = require('../helpers')

module.exports = async ({ variant, issuer }) => {
  return assert.doesNotReject(
    helpers.greenPath({
      issuer,
      variant,
      forceNonce: true
    })
  )
}
