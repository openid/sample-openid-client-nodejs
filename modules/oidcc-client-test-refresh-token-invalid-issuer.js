const { strict: assert } = require('assert')

const helpers = require('../helpers')

module.exports = async ({ variant, issuer }) => {
  const regularflow = helpers.greenPath({
    issuer,
    variant,
    skipUserinfo: true
  })

  await assert.doesNotReject(regularflow)
  const { client, tokens } = await regularflow

  return assert.rejects(
    helpers.refreshToken({ client, tokens }),
    {
      name: 'RPError',
      message: /^unexpected iss value, expected (?<issuer>.+), got: \k<issuer>1$/
    }
  )
}
