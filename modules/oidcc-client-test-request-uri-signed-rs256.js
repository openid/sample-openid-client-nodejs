const { strict: assert } = require('assert')

const helpers = require('../helpers')

module.exports = async ({ variant, issuer }) => {
  let clonedVariant = { ... variant };
  clonedVariant.request_type='request_uri';
  return assert.doesNotReject(
    helpers.greenPath({
      issuer,
      variant: clonedVariant,
      metadata: {
        request_object_signing_alg: 'RS256'
      }
    })
  )
}
