const { strict: assert } = require('assert')

const helpers = require('../helpers')

module.exports = async ({ variant, issuer }) => {
  const { tokens, claims, userinfo } = await helpers.greenPath({
    issuer,
    variant,
    params: {
      scope: 'openid email'
    }
  })

  if (tokens.access_token) {
    assert(userinfo.email)
  } else {
    assert(claims.email)
  }
}
