const { strict: assert } = require('assert')

const helpers = require('../helpers')

module.exports = async ({ variant, issuer }) => {
  return assert.rejects(
    helpers.greenPath({
      issuer,
      variant
    }),
    {
      name: 'RPError',
      message: /^userinfo sub mismatch, expected (?<sub>.+), got: \k<sub>invalid$/
    }
  )
}
