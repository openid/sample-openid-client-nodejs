/* eslint-disable no-await-in-loop */
/* eslint-env mocha */

const { strict: assert } = require('assert')
const { existsSync } = require('fs')
const path = require('path')
const util = require('util')

const open = require('open')

const debug = require('./debug')
const API = require('./api')

const {
  ALIAS = 'openid-client',
  CLIENT_AUTH_TYPE = 'client_secret_basic',
  PLAN_NAME = 'oidcc-client-test-plan',
  REQUEST_TYPE = 'plain_http_request',
  RESPONSE_MODE = 'default',
  RESPONSE_TYPE = 'code',
  SUITE_ACCESS_TOKEN,
  SUITE_BASE_URL,
  ONLY,
  OPEN_PLAN,
  OPEN_MODULE,
  CLIENT_CERT,
  CLIENT_CERT_KEY
} = process.env;

(async () => {
  assert(ALIAS, 'process.env.ALIAS missing')
  assert(CLIENT_AUTH_TYPE, 'process.env.CLIENT_AUTH_TYPE missing')
  assert(PLAN_NAME, 'process.env.PLAN_NAME missing')
  assert(REQUEST_TYPE, 'process.env.REQUEST_TYPE missing')
  assert(RESPONSE_MODE, 'process.env.RESPONSE_MODE missing')
  assert(RESPONSE_TYPE, 'process.env.RESPONSE_TYPE missing')
  assert(SUITE_BASE_URL, 'process.env.SUITE_BASE_URL missing')

  const variant = {
    client_auth_type: CLIENT_AUTH_TYPE,
    request_type: REQUEST_TYPE,
    response_type: RESPONSE_TYPE,
    client_registration: 'dynamic_client',
    response_mode: RESPONSE_MODE
  }

  const configuration = {
    alias: ALIAS,
    description: 'test suite runner for openid-client',
    waitTimeoutSeconds: 2
  }

  const runner = new API({ baseUrl: SUITE_BASE_URL, bearerToken: SUITE_ACCESS_TOKEN })

  let PLAN_ID
  let MODULES

  if (!process.env.PLAN_ID) {
    const plan = await runner.createTestPlan({
      configuration,
      planName: PLAN_NAME,
      variant
    });

    ({ id: PLAN_ID, modules: MODULES } = plan)

    debug('Created test plan, new id %s', PLAN_ID)
  } else {
    ({ PLAN_ID } = process.env)

    const { modules } = await runner.getTestPlan({
      planId: PLAN_ID
    })

    MODULES = modules.map((module) => module.testModule)
    debug('Loaded test plan id %s', PLAN_ID)
  }

  const planDetail = util.format('%s/plan-detail.html?plan=%s', SUITE_BASE_URL, PLAN_ID)

  debug(planDetail)
  debug('modules %O', MODULES)

  if (OPEN_PLAN) {
    after(() => open(planDetail, { wait: false }))
  }

  for (const moduleObject of MODULES) { // eslint-disable-line no-restricted-syntax
    const moduleName=moduleObject.testModule;
    const moduleFile = path.join(__dirname, 'modules', `${moduleName}.js`)
    if (existsSync(moduleFile) && (!ONLY || ONLY === moduleName)) {
      it(moduleName, async function () {
        const moduleDefinition = require(moduleFile)
        debug('Running test module: %s', moduleName)
        const { id: moduleId } = await runner.createTestFromPlan({ plan: PLAN_ID, test: moduleName })
        debug('Created test module, new id: %s', moduleId)
        const moduleUrl = util.format('%s/log-detail.html?log=%s', SUITE_BASE_URL, moduleId)
        debug(moduleUrl)
        if (OPEN_MODULE) {
          await open(moduleUrl, { wait: false })
        }
        await runner.waitForState({ moduleId, timeout: 5 * 1000, interval: 100, states: new Set(['WAITING']), results: new Set() })
        debug('Running %s', moduleFile)
        return Promise.all([
          runner.waitForState({ moduleId }),
          moduleDefinition({ variant, issuer: `${SUITE_BASE_URL}/test/a/${ALIAS}/` })
        ])
      })
    } else {
      it.skip(moduleName)
    }
  }

  run()
})().catch((err) => {
  process.exitCode = 1
  debug(err)
})
